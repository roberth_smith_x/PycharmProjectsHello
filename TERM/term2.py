
import serial
import serial.tools.list_ports
import time
import sys

# enum ports
ports = serial.tools.list_ports.comports()
for port in ports:
    print(port.device)
#COM5
#COM4

#open

port_ = "COM4"

if len(sys.argv) > 1:
    port_ = sys.argv[1]

pt_sets = {
    'port': port_,
    'baudrate': 9600,
    'bytesize': 8,
    'parity': "N",
    'stopbits': 1,
    'timeout': 5,
    'xonxoff': False,
    'rtscts': False,
}
print("> WANTS TO OPEN: %s\n" % pt_sets)

ser = serial.Serial(**pt_sets)


print("+ WAS OPENED: %s\n" % ser)

#write
msg = b"Help!!!"
print("> WANTS TO WRITE TO %s: %s\n" % (ser.port, msg.decode()))

wrt_bt_cnt = ser.write(msg)

print("+ WAS WRITTEN: %d bytes\n" % wrt_bt_cnt)

#read
print("> WAITS TO READ FROM %s\n" % ser.port)
while True:
    line = ser.readline()
    
    if len(line) > 0: 
        print("\n+ WAS READ: %s\n", line.decode())
        exit()
        
    print("*")
    time.sleep(1)
    
    