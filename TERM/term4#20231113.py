import serial
import serial.tools.list_ports
import time
import sys
import argparse

# --- get existing ports

ports = serial.tools.list_ports.comports()
print("> FOUND PORTS: %s" % ", ".join([p.name for p in ports]))
    
# --- set input arguments format 
try:
    arg_parser = argparse.ArgumentParser()
    # positional args
    arg_parser.add_argument('port', 
        help="Port name (%s)" % ports[0].name if len(ports)>0 else '',
        choices=[p.name for p in ports],
        default=ports[0].name if len(ports)>0 else '',
        nargs='?',
        )
        
    arg_parser.add_argument('baudrate',
        help="Baudrate in kb/s (%s)" % list(serial.Serial.BAUDRATES)[-1],
        choices=list(serial.Serial.BAUDRATES),
        default=list(serial.Serial.BAUDRATES)[-1],
        nargs='?',
        type=int,
        )
    
    bytesizes = serial.Serial.BYTESIZES #range(5, 9)
    parities = serial.Serial.PARITIES #['N', 'P', 'O', 'E', 'M',]
    stopbitses = serial.Serial.STOPBITS #[0, 1,]
    
    datafmts = []
    for b in bytesizes:
        for p in parities:
            for s in stopbitses:                
                datafmts.append("%d%s%d" % (b, p, s)) 
    
    arg_parser.add_argument('datafmt',
        help="Data Format as (8N1)",
        choices=datafmts,
        default='8N1',
        nargs='?',
        )
        
    arg_parser.add_argument('xonxoff',
        help="XONXOFF as (0)",
        choices=[0, 1],
        default=0,
        nargs='?',
        )
        
    arg_parser.add_argument('rtscts',
        help="RTSCTS as (0)",
        choices=[0, 1],
        default=0,
        nargs='?',
        )
        
except Exception as e:
    print("> ERR: ", str(e))

finally:
    if (len(ports) == 0):
        print("> ERR: NO SERIAL PORTS FOUND! (Please, verify if serial ports exist)")
        exit()
    
    
# --- get arguments from current command line
get_port_args = arg_parser.parse_args()
print("> GET PORT ARGS = %s" % vars(get_port_args))

set_port_params = {}

for key, val in vars(get_port_args).items():
    if key == 'xonxoff' or key == 'rtscts':
        set_port_params[key] = bool(val)
    else:
        if key != 'datafmt':
            set_port_params[key] = val
        else:
            set_port_params[bytesize] = datafmt[0]
            set_port_params[parity] = datafmt[1]
            set_port_params[stopbits] = datafmt[2]
#?
#get_port_args.datafmt = '8N1' => 
 
#set_port_params{
#    'baudrate': 9600,
#    'bytesize': 8,
#    'parity': "N",}


# --- open predefined port

"""
set_port_params = {
    'port': port_,
    'baudrate': 9600,
    'bytesize': 8,
    'parity': 'N',
    'stopbits': 1,
    'xonxoff': False,
    'rtscts': False,
    'timeout': 5,
}
"""

print("> WANTS TO OPEN (SET PORT PARAMS): %s\n" % set_port_params)

exit()

try:
    ser = serial.Serial(**set_port_params)
    print("+ WAS OPENED: %s\n" % ser)

    # --- input from user and write to port
    
    msg = bytes(input(), 'utf-8')
    print("> WANTS TO WRITE TO %s: %s\n" % (ser.port, msg.decode(encoding='utf-8')))

    wrt_bt_cnt = ser.write(msg)

    print("+ WAS WRITTEN: %d bytes\n" % wrt_bt_cnt)

    
    print("> WAITS TO READ FROM %s\n" % ser.port)
    while True:
        # --- wait any bytes will be received on port
        
        line = ser.readline()
    
        if line:
            print("\n+ WAS READ: %s\n", line.decode())

        print("*", end="")
        
except serial.SerialException as se:
    print("Serial port error:", str(se))

except KeyboardInterrupt:
    pass

except Exception as e:
    print("An error occurred:", str(e))
    
finally:
    if ser.is_open:
        ser.close()
        print("Serial connection closed.")
    
    