import serial
import serial.tools.list_ports
import time
import sys
import argparse

# --- CONSTANTS
READ_TO = 3

# --- get existing ports

ports = serial.tools.list_ports.comports()
print("> FOUND PORTS: %s" % ", ".join([p.name for p in ports]))

# --- set input arguments format 
# ! add both type arguments - position + optional
try:
    arg_parser = argparse.ArgumentParser()
    # positional args
    arg_parser.add_argument('use_port', 
        help="Port name (%s)" % ports[0].name if len(ports)>0 else '',
        choices=[p.name for p in ports],
        default=ports[0].name if len(ports)>0 else '',
        nargs='?',
        )
    arg_parser.add_argument('-p', '--port',
        help="Port name (%s)" % ports[0].name if len(ports)>0 else '',
        choices=[p.name for p in ports],
        #default=ports[0].name if len(ports)>0 else '',
        required=False,
        )
       
    arg_parser.add_argument('use_baudrate',
        help="Baudrate in kb/s (%s)" % list(serial.Serial.BAUDRATES)[-1],
        choices=list(serial.Serial.BAUDRATES),
        default=list(serial.Serial.BAUDRATES)[-1],
        nargs='?',
        type=int,
        )
    arg_parser.add_argument('-b', '--baudrate',
        help="Baudrate in kb/s (%s)" % list(serial.Serial.BAUDRATES)[-1],
        choices=list(serial.Serial.BAUDRATES),
        #default=list(serial.Serial.BAUDRATES)[-1],
        required=False,
        type=int,
        )
    
    bytesizes = serial.Serial.BYTESIZES #range(5, 9)
    parities = serial.Serial.PARITIES #['N', 'P', 'O', 'E', 'M',]
    stopbitses = serial.Serial.STOPBITS #[0, 1,]
    
    datafmts = []
    for b in bytesizes:
        for p in parities:
            for s in stopbitses:                
                datafmts.append("%d%s%d" % (b, p, s)) 
    
    arg_parser.add_argument('use_datafmt',
        help="Data Format as (8N1)",
        choices=datafmts,
        default='8N1',
        nargs='?',
        )
    arg_parser.add_argument('-d', '--datafmt',
        help="Data Format as (8N1)",
        choices=datafmts,
        #default='8N1',  
        required=False,
        )
        
    arg_parser.add_argument('use_xonxoff',
        help="XONXOFF as (0)",
        choices=[0, 1],
        default=0,
        type=int,
        nargs='?',
        )
    arg_parser.add_argument('-x', '--xonxoff',
        help="XONXOFF as (0)",
        choices=[0, 1],
        #default=0,
        type=int,
        required=False,
        )
        
    arg_parser.add_argument('use_rtscts',
        help="RTSCTS as (0)",
        choices=[0, 1],
        default=0,
        type=int,
        nargs='?',
        )
    arg_parser.add_argument('-r', '--rtscts',
        help="RTSCTS as (0)",
        choices=[0, 1],
        #default=0,
        type=int,
        required=False,
        )
        
except Exception as e:
    print("> ERR: ", str(e))

finally:
    if (len(ports) == 0):
        print("> ERR: NO SERIAL PORTS FOUND! (Please, verify if serial ports exist)")
        exit()
    
    
# --- get arguments from current command line
get_port_args = arg_parser.parse_args()
print("> GET PORT ARGS = %s" % vars(get_port_args))

# основной набор аргументов - позиционные
get_port_args_pri = {}
# дополнительный набор аргументов - опциональные
get_port_args_alt = {}


# split positional args from optional by prefix

args_pref = 'use_'

get_port_args_pri_keys = list(filter(lambda x : x.startswith(args_pref), vars(get_port_args).keys()))
get_port_args_pri = {key.replace(args_pref, '') : vars(get_port_args)[key] for key in get_port_args_pri_keys}
get_port_args_alt = {key : vars(get_port_args)[key] for key in vars(get_port_args).keys() - get_port_args_pri_keys}

# merge optional args with positional
# if optional arg is not None, then it replace correspondent positional arg!

set_port_params = {}

#for key, val in vars(get_port_args).items():
for key, val in get_port_args_pri.items(): 
    if get_port_args_alt[key] is not None:
        val = get_port_args_alt[key]
        
    if key == 'xonxoff' or key == 'rtscts':
        set_port_params[key] = bool(val)
    else:
        if key != 'datafmt':
            set_port_params[key] = val
        else: # vars(get_port_args).items()['datafmt'] = val
            set_port_params['bytesize'] = int(val[0])
            set_port_params['parity'] = val[1]
            set_port_params['stopbits'] = int(val[2])



set_port_params['timeout'] = READ_TO

print("> WANTS TO OPEN (SET PORT PARAMS): %s\n" % set_port_params)

# --- use online terminal in synchronous mode

try:
    ser = serial.Serial(**set_port_params)
    print("+ WAS OPENED: %s\n" % ser)

    # --- input from user and write to port
    
    msg = bytes(input(), 'utf-8')
    print("> WANTS TO WRITE TO %s: %s\n" % (ser.port, msg.decode(encoding='utf-8')))

    wrt_bt_cnt = ser.write(msg)

    print("+ WAS WRITTEN: %d bytes\n" % wrt_bt_cnt)

    
    print("> WAITS TO READ FROM %s\n" % ser.port)
    while True:
        # --- wait any bytes will be received on port
        
        line = ser.readline()
    
        if line:
            print("\n+ WAS READ: %s\n", line.decode())

        print("*", end="")
        
except serial.SerialException as se:
    print("Serial port error:", str(se))

except KeyboardInterrupt:
    pass

except Exception as e:
    print("An error occurred:", str(e))
    

    
    