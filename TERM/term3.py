
import serial
import serial.tools.list_ports
import time
import sys


# find port from available 
# to use it as default

ports = serial.tools.list_ports.comports()
port_ = ports[0]
if port_ is None:
    raise ValueError("No COM port found.")

# else if port was was set as command line argument
# use it

if len(sys.argv) > 1:
    port_ = sys.argv[1]
    
# open predefined port

pt_sets = {
    'port': port_,
    'baudrate': 9600,
    'bytesize': 8,
    'parity': "N",
    'stopbits': 1,
    'timeout': 5,
    'xonxoff': False,
    'rtscts': False,
}
print("> WANTS TO OPEN: %s\n" % pt_sets)

try:
    ser = serial.Serial(**pt_sets)
    print("+ WAS OPENED: %s\n" % ser)

    #write
    msg = b"Help!!!"
    print("> WANTS TO WRITE TO %s: %s\n" % (ser.port, msg.decode()))

    wrt_bt_cnt = ser.write(msg)

    print("+ WAS WRITTEN: %d bytes\n" % wrt_bt_cnt)

    #read
    print("> WAITS TO READ FROM %s\n" % ser.port)
    while True:
        line = ser.readline()
    
        if line:
            print("\n+ WAS READ: %s\n", line.decode())

        print("*", end="")
        
except serial.SerialException as se:
    print("Serial port error:", str(se))

except KeyboardInterrupt:
    pass

except Exception as e:
    print("An error occurred:", str(e))
    
finally:
    if ser.is_open:
        ser.close()
        print("Serial connection closed.")
    
    