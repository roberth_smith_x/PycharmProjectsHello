import asyncio
#from aioconsole import ainput
#https://stackoverflow.com/questions/61288221/move-the-cursor-back-for-taking-input-python

def printloop():
    
    return print(' РОБОТА ОТВЕТ))) ', end='')
 
 
    
async def print_sym():

    #print("> call print_sym()\n")
    while True:
        #print('*', end='')
        #print("\rwaiting...(%03d)>>>" % cnt, end="")
         
        loop = asyncio.get_event_loop()
        content = loop.run_in_executor(None, printloop) 
        await asyncio.sleep(1)
     

async def user_input():

    while True:
            
            loop = asyncio.get_event_loop()
            content = await loop.run_in_executor(None, input, "")
            print ("\033[A\033[A\033[A")
            print(">>> " + content, end="")           
            print("\n\r<<< \033[K\n\n\n>>> ", end="")


async def main():

    #print("> call main()\n")
    print("\n\n>>> ", end="")
    tasks = [user_input(), print_sym()]
    await asyncio.gather(*tasks)
    
    #ОТПРАВЛЯЕМ ПРИВЕТСТИЕ - ПРИВЕТ


print("> START\n")

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
	
print("> END\n")
    
	