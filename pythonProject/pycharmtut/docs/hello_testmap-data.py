#=== hello_testmap-data.sch.txt
#=== Структура данных для подзадачи вывода иерархического меню treeview

#--- Простой лист с вертикальной иерархией

['grandmother', 'mother', 'son',]
 
# Тест данные для иерархической структуры дерево

data = [
 {	'id': 1, 	'name': 'grandmother',	'level': 0, 'parent': 0 },
 {	'id': 2, 	'name': 'mother',	'level': 1, 'parent': 1  },
 {	'id': 4, 	'name': 'uncle',	'level': 1, 'parent': 1  },
 { 	'id': 3, 	'name': 'son',	'level': 2, 'parent': 2, },
]


"""
1-st way - show simplest hierarchy with levels and levels members
 
['grandmother']
- ['mother', 'uncle']
-- ['son']   
"""
 
arch = {}
for d in data:
	lev = d['level']
	par = d['parent']
	if lev not in arch:
		arch[lev] = {}
	if par not in arch[lev]:
		arch[lev][par] = []
	arch[lev][par].append(d['name'])
		
print('1-st way - show simplest hierarchy with levels and levels members\n')
for lev in arch:
	for par in arch[lev]:
		print('-' * lev, arch[lev][par])
	
	
"""
2-nd way - show real hierarchy treeview


└──grandmother
  └──mother
    └──son
  └──uncle
"""
#recoursive



arch = {}

for d in data:
	lev = d['level']
	par = d['parent']
	if lev not in arch:
		arch[lev] = {}
	if par not in arch[lev]:
		arch[lev][par] = {}
	
	arch[lev][par][d['id']] = d

	# { { { [{}] } } }
	# 1-st idx lev = range(0, 2)
    # 2-nd idx par = any from ids 
    # 3-nd idx id = any from ids

def showNode(lev, par, id, nodes):
	# has_sibling = lev + 1 in nodes and id in nodes[lev + 1] and nodes[lev + 1][id]
	# вывод в консоли
	print('  ' * lev + '└' + '──' + nodes[lev][par][id]['name'])
	if lev + 1 in nodes and id in nodes[lev + 1]:
		for sibling_id in nodes[lev + 1][id]:
			sibling = nodes[lev + 1][id][sibling_id]
			showNode(sibling['level'], sibling['parent'], sibling['id'], nodes)


'''

<ul class="tree">
  <li>
    <details open>
      <summary>grandmother</summary>
      <ul>
        <li>
          <details>
            <summary>mother</summary>
            <ul>
              <li>son</li>
            </ul>
          </details>
        </li>
        <li>
          <details>
            <summary>uncle</summary>
          </details>
        </li>     
      </ul>
    </details>
  </li>
</ul>



<ul>
  <li>
    GRANDMOTHER
      <ul>
        <li>
          MOTHER
		</li>
		<li1>
          UNCLE
		</li1>
	  </ul1>
  </li0>
</ul0>

<ul>
	<li>
	grandmother
		<ul>
			<li>
			mother
				<ul>
					<li>
					son
					</li>
				</ul>
			</li>
			<li>
			uncle
			</li>
		</ul>
	</li>
</ul>

'''


def showNodeHtml(lev, par, id, nodes, prevlev):
	#has_sibling = lev + 1 in nodes and id in nodes[lev + 1] and nodes[lev + 1][id]
	# вывод в консоли
    # print('  ' * lev + '└' + '──' + nodes[lev][par][id]['name'])
    
    # вывод в html Из примера https://iamkate.com/code/tree-views/

	# вывести открывающий список
	# если элемент первый в списке себе подобных - одноуровных (mo)
	if id == list(nodes[lev][par])[0]:
		print('<ul>')
	print('<li>')
	print(nodes[lev][par][id]['name'] )

	if lev + 1 in nodes and id in nodes[lev + 1]:
		for sibling_id in nodes[lev + 1][id]:
			sibling = nodes[lev + 1][id][sibling_id]
			showNodeHtml(sibling['level'], sibling['parent'], sibling['id'], nodes, lev)

	print('</li>')

	# вывести закрывающий список
	# если элемент последний в списке себе подобных - одноуровных (mo)
	if id == list(nodes[lev][par])[-1]:
		print('</ul>')

# Использование рекурсивной функции по выводу дерева
#print('2-nd way - show real hierarchy treeview\n')
#showNode(0, 0, 1, arch)

html_string = showNodeHtml(0, 0, 1, arch, 0)
	
	