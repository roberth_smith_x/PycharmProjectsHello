--- SHOP-DJANGO#3.TSK.TXT


	3.1 В Django (той версии 2.2, что в django-cart-demo проекте) 
		создать полный sitemap - т.е. карту почти всех статических веб-ссылок проекта django-cart-demo.
		
		3.1.1. Sitemap должен быть построен иерархически, 
			в виде дерева с единственным корнем.
			
		В текстом виде его представление напоминает что-то в духе
		(мнемосхема - пример, ссылки не все):
			
		<a href="/">Home</a>
		├───<a href="/core">Products</a>
		│   ├───<a href="/core/category_list">Categories</a>
		│   ├───<a href="/core/register">Register</a>
		│   └───<a href="/core/logout">Logout</a>
		└───<a href="/admin">Admin</a>
			├───<a href="/admin/login">Login</a>
			
		И далее пошли ссылки в разделе /admin/xxxx
		Все эти ссылки - перечислены в файле site-packages/django/contrib/admin/sites.py
			внутри функции def get_urls(self): 
				переменная urlpatterns
			
			
		Вот все ссылки 
		
		
				Публичные ссылки (ссылки frontend)
		--- index
	
		/ => templates/index.html
			Home
			
		--- core 
		
		/core => core/templates/core/product_list.html
		
		/core/category_list => core/templates/core/category_list.html
		
		/core/register => core/templates/core/register.html
		
		/core/logout => ''
		
		/core => core/product_list.html
			
		/core/create => 
					venv/Lib/site-packages/django/contrib/admin/templates/admin/change_form.html
		
		/core/order_summary => core/templates/core/order_summary.html
		/core/user_login => core/templates/core/login.html
		
		/core/sitemap => core/templates/core/sitemap.html
	
		
		--- admin
		
		/admin => venv/Lib/site-packages/django/contrib/admin/templates/admin/index.html
		
		/admin/login/ => venv/Lib/site-packages/django/contrib/admin/templates/admin/login.html
		
		
		/admin/logout/ => ?
		
		/admin/password_change => ? 
		
		/admin/password_change/done/ => ?
		
		---
		/admin/auth/user/
			Просмотр
		
		/admin/auth/user/add/ => 		
				venv/Lib/site-packages/django/contrib/admin/templates/admin/change_form.html
				venv/Lib/site-packages/django/contrib/admin/templates/admin/auth/user/add_form.html
			Добавление
			
			
		
		
	
		3.1.2. Но по заданию, нам нужно вывести этот sitemap в виде веб-верстки,
		такой же, какая была сделана было в предыдущей задаче YOURSHOP#2
		Там выводили дерево иерархических данных словаря data при помощи функции showNodeHtml()
		И эту функцию вызывали рекурсивно в представлении hello/views.py 
			def sportmap(request)
			
		3.1.3. Поэтому нужно внутри сайта проекта django-cart-demo
			создать новую ссылку /sitemap (в /core/urls.py) и направить при вводе этой ссылки 
			в браузере на новый веб-шаблон в папке core/templates/core/sitemap.html, где и будет рисоваться дерево иерархических ссылок
			при помощи функции showNodeHtml() из иерархических данных data
			Для этого добавить обработчик - внутри модуля /core/views.py
			добавить функцию sitemap() - это обработчик.
			Она вызывает showNodeHtml() и возвращает htmlstring
			которые передает как параметр (htmlstring) внутрь шаблона core/templates/core/sitemap.html
			
			
		3.1.4 Сам шаблон templates/sitemap.html - такой же простой по смыслу, что в задании YOURSHOP#2
			(как templates/sitemap.hmtl)
			
		3.1.5 Пример структуры иерархических данных:
			
		
			data = [
				{ 'id': 1, 'name': '<a href="/">Home</a>', 'level': 0, 'parent': 0 },
				{ 'id': 2, 'name': 'Products', 'level': 1, 'parent': 1 },
				
			]
			
			При заполнении структуры data
			нужны Наименования для страниц сайта,
			т.е. то, что есть параметр name в data
			т.е. то, что в мнемосхеме внутри <a></a> 
			(например <a href="/">Home</a>)
			и сами ссылки на эти страницы
			
			Но брать это нужно либо из меню сайта либо из тега страниц <h1>)
			
			
		Алгоритм
			#1 Взять список всех ссылок из 3.1.1
			
			#2 Взять заполнить строчки для мнемосхемы таким образом (пример)
			Тег <a href=""> ведет на страницу сайта 
			В ней внутри тега <a> идет наименование страницы
			
			пример 
			<a href="/">Home</a>
			<a href="/core">Products</a>
			
			#3 Заполнить иерархическую структуру data 3.1.5
			
			#4 Добавить в /core/urls.py строку для sitemap.html 3.1.3
			
			#5 Добавить файл шаблона sitemap.html 3.1.4
			
			#6 Добавить ф-цию showNodeHtml()
			
			#7 Добавить ф-цию обработчика sitemap() 3.1.3
			
			#8 Проверить работу введя в браузере /core/sitemap
			Должна отобразить карта сайта в виде дерево
			
			
		* сделать потом
		
		3.1.6 Так как часть ссылок sitemap будет вести внутрь защищенной части веб-сайта (бекенда-админки),
			то эти ссылки в sitemap нужно будет показывать только тем пользователям сайта,
			у которых есть права администратора на веб-сайте. 
			Т.е. только тем пользователям, которые после логина на веб-сайте могут заходить в раздел /admin
			
			Для этого нужно будет дополнительно проверять права зарегистрировавшегося пользователя веб-сайта.
			Будут использованы возможности пакета 'django.contrib.admin'
		
--- Примечания к задаче
			
* Посмотреть содержимое выполненных задач YOURSHOP#2.TSK.TXT YOURSHOP#3.TSK.TXT
			
* Описание диспетчера ссылок в Django 2.2
	https://docs.djangoproject.com/en/2.2/topics/http/urls/
			
* Информация по встроенному языку шаблонов шаблонизатора Django
	https://docs.djangoproject.com/en/2.2/ref/templates/language/
	
* Описание интерфейса админки 'django.contrib.admin' - очень детальное описание
	Здесь нужно просто найти и понять, как проверять права зарегистрировавшегося и залогиненного пользователя
	https://docs.djangoproject.com/en/2.2/ref/contrib/admin/#django.contrib.admin.autodiscover