from django.shortcuts import render

data = [
    {'id': 1, 'name': 'grandmother', 'level': 0, 'parent': 0},
    {'id': 2, 'name': 'mother', 'level': 1, 'parent': 1},
    {'id': 4, 'name': 'uncle', 'level': 1, 'parent': 1},
    { 'id': 3, 	'name': 'son',	'level': 2, 'parent': 2, },
]

data = [   {'id': 1, 'name': 'types of sports', 'level': 0, 'parent': 0},
   {'id': 2, 'name': 'summer types', 'level': 1, 'parent': 1},
   {'id': 3, 'name': 'winter types', 'level': 1, 'parent': 1},
           {'id': 4, 'name': 'without ball', 'level': 2, 'parent': 2},
   {'id': 5, 'name': 'running', 'level': 3, 'parent': 4},
           {'id': 6, 'name': 'with ball', 'level': 2, 'parent': 2},
   {'id': 7, 'name': 'voleyball', 'level': 3, 'parent': 6},
           {'id': 8, 'name': 'football', 'level': 3, 'parent': 6},
   {'id': 9, 'name': 'beach football', 'level': 4, 'parent': 8},
]

"""
2-nd way - show real hierarchy treeview


└──grandmother
  └──mother
    └──son
  └──uncle
"""
# recoursive



arch = {}

for d in data:
    lev = d['level']
    par = d['parent']
    if lev not in arch:
        arch[lev] = {}
    if par not in arch[lev]:
        arch[lev][par] = {}

    arch[lev][par][d['id']] = d

# { { { [{}] } } }
# 1-st idx lev = range(0, 2)
# 2-nd idx par = any from ids
# 3-nd idx id = any from ids

html_string = ''
def showNodeHtml(lev, par, id, nodes):

    global html_string
    # has_sibling = lev + 1 in nodes and id in nodes[lev + 1] and nodes[lev + 1][id]
    # вывод в консоли
    # print('  ' * lev + '└' + '──' + nodes[lev][par][id]['name'])

    # вывод в html Из примера https://iamkate.com/code/tree-views/

    # вывести открывающий список
    # если элемент первый в списке себе подобных - одноуровных (mo)

    pad = '  '
    pad_factor = 4 * lev

    if id == list(nodes[lev][par])[0]:
        # tree-padding tree-vertical-lines tree-horizontal-lines tree-summaries
        html_string += pad * pad_factor \
                       + '<ul{0}>'.format(' class="tree "' if lev == 0 else '') + '\n'

    html_string += pad * (pad_factor + 1) + '<li>' + '\n'

    if True or id == list(nodes[lev][par])[0]:
        if (lev + 1 in nodes and id in nodes[lev + 1]):
            html_string += pad * (pad_factor + 2) + '<details open>' + '\n'
        else:
            html_string += pad * (pad_factor + 2) + '<details>' + '\n'

    if True or (lev + 1 in nodes and id in nodes[lev + 1]):
        html_string += pad * (pad_factor + 3) + '<summary>{0}</summary>'.format(nodes[lev][par][id]['name']) + '\n'
    else:
        html_string += pad * (pad_factor + 3) + nodes[lev][par][id]['name'] + '\n'

    if lev + 1 in nodes and id in nodes[lev + 1]:
        for sibling_id in nodes[lev + 1][id]:
            sibling = nodes[lev + 1][id][sibling_id]
            showNodeHtml(sibling['level'], sibling['parent'], sibling['id'], nodes)

    if True or id == list(nodes[lev][par])[-1]:
        if True or (lev + 1 in nodes and id in nodes[lev + 1]):
            html_string += pad * (pad_factor + 2) + '</details>' + '\n'

    html_string += pad * (pad_factor + 1) + '</li>' + '\n'

    # вывести закрывающий список
    # если элемент последний в списке себе подобных - одноуровных (mo)
    if id == list(nodes[lev][par])[-1]:
        html_string += pad * pad_factor + '</ul>' + '\n'

# Использование рекурсивной функции по выводу дерева
# print('2-nd way - show real hierarchy treeview\n')
# showNode(0, 0, 1, arch)

# Create your views here.

def index(request):

    return render(request, 'hello/index.html')


def sportmap(request):

    return render(request, 'hello/sportmap.html')


def testmap(request):

    global html_string
    html_string = ''
    showNodeHtml(0, 0, 1, arch)

    # использовать файл hello_testmap-data.py
    return render(request, 'hello/testmap.html', {'html_string': html_string})